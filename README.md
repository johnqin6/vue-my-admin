# vue-my-admin

## 项目描述
**一个基于（vue + vue-router + vuex + axios）vue全家桶 + elementUI框架开发的后台管理系统，
因为该项目主要练习前端技术，所以并没有使用node.js等语言操作数据库，而是使用mock.js来模拟数据。**

## 项目功能介绍
+ 登录功能
+ 导航功能
+ 数据新增，查看，编辑，删除功能。
+ 数据筛选功能
+ echarts图表展示功能

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
