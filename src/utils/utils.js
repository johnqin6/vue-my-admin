const crypto = require('crypto');
export default {
    //问候方法
    getTimeGreeting(){
        let date = new Date();
        let hours = date.getHours();
        if(hours >= 0 && hours < 8){
            return '早上好';
        }else if(hours >= 8 && hours < 12){
            return '上午好';
        }else if(hours >= 12 && hours < 14){
            return '中午好';
        }else if(hours >= 14 && hours < 19){
            return '下午好';
        }else{
            return '晚上好';
        }
    },
    //md5加密，参数：加密字符；
    CalcuMD5(val){
        let md5 = crypto.createHash('md5');
        md5.update(val);
        return md5.digest('hex');
    },
    //获得时间戳 参数：时间对象；
    getTime(val){
        let timestamp = (new Date(val)).getTime();
        return timestamp;
    },
    //不满二位添零
    padDate(val){
        let newVal = val < 10 ? '0' + val : val;
        return newVal;
    },
    //时间格式转换 参数1：字符串, 参数2：‘-’，‘/’，参数3：'date','datetime','time'
    formatTime(time,split,type){
        let date = new Date(time);
        let year = date.getFullYear();
        let month = this.padDate(date.getMonth() + 1);
        let day = this.padDate(date.getDate());
        let hours = this.padDate(date.getHours());
        let mintues = this.padDate(date.getMinutes());
        let seconds = this.padDate(date.getSeconds());
        if(split === '/'){
            if(type === 'date'){
                return year + '/' + month + '/' + day;
            }else if(type === 'time'){
                return hours + ':' + mintues + ':' + seconds; 
            }else{
                return year + '/' + month + '/' + day + ' ' + hours + ':' + mintues + ':' + seconds;
            }
        }else{
            if(type === 'date'){
                return year + '-' + month + '-' + day;
            }else if(type === 'time'){
                return hours + ':' + mintues + ':' + seconds;
            }else{ 
                return year + '-' + month + '-' + day + ' ' + hours + ':' + mintues + ':' + seconds;
            }
        }
    },
    //根据数组对象值排序，比较函数 参数：属性名
    compare(key){
        return function(a,b) {
            var val1 = a[key];
            var val2 = b[key];
            return val2 - val1;
        }
    },
    //根据数组的属性名排序 参数：数组，属性名
    objinArrSort(arr,key){
        let _arr = arr;
        if(key === 'creaton' || key === 'birth'){
            _arr = _arr.map(item => {
                item[key] = this.getTime(item[key]);
                return item;
            });
            _arr.sort(this.compare(key));
            _arr = _arr.map(item => {
                item[key] = this.formatTime(item[key]);
                return item;
            });
        }else{
            _arr.sort(this.compare(key));
        }
        return _arr;
    },
    //生成四位验证码 Captcha
    createCaptcha(){
        let arr = [];
        let Captcha = [];
        for(let i = 0; i < 10; i++){
            arr.push(i);
        }
        for(let i = 65; i < 91; i++){
            arr.push(String.fromCharCode(i));
        }
        for(let i = 97; i < 123; i++){
            arr.push(String.fromCharCode(i));
        }
        while(Captcha.length < 4){
            Captcha.push(arr[Math.floor(Math.random() * arr.length)]);
        }
        return Captcha;
    }
};