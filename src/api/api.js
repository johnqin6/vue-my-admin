import axios from 'axios';
let base = "";

//获取验证码
export const getCaptcha = params => { return axios.get(`${base}/getCaptcha`, params).then(res => res.data)};
//登录用户信息
export const getLoginUsers = (params) => { return axios.post(`${base}/login`, params).then(res => res.data)};

//用户列表信息
export const getUserListData = (params) => { return axios.get(`${base}/getUserListData`, { params: params }).then(res => {
    return res;
})};

//删除用户
export const delUserData = (params) => { return axios.get(`${base}/delUserData`, { params: params })};

//获得用户数据
export const getUserData = (params) => { return axios.get(`${base}/getUserData`, { params: params }).then(res => res.data)};

//新增用户数据
export const addUserData = (params) => { return axios.post(`${base}/addUserData`, params).then(res => res.data)};

//编辑用户数据
export const editUserData = (params) => { return axios.post(`${base}/editUserData`, params).then(res => res.data)};

//获取城市数据
export const getCitysData = (params) => { return axios.get(`${base}/getCitysData`, { params: params }).then(res => {
   return res.data
}).catch(err => {
    console.log(err);
})};

//获取全部城市数据
export const getAllCitysData = params => {return axios.get(`${base}/getAllCitysData`, params).then(res => res.data)};
//密码的修改
export const editPassword = (params) => { return axios.post(`${base}/editPassword`, params).then(res => res.data)};