import Vue from 'vue';
import Vuex from 'vuex';
import { getLoginUsers } from '../api/api';
Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        // token: getToken(),
        loginUser: {
            id: '',
            username: '',
            password: '',
            rname: '', //真实姓名
            role: '', //权限
            avatar: ''
        },
        userData: []
    },
    mutations: {
        caveUserNameAndPwd(state,payload){
            state.loginUser.username = payload.username;
            state.loginUser.password = payload.password;
        },
        SET_TOKEN: (state, token) => {
            state.token = token
        },
    },
    actions: {
        getLoginUser(context,param){
            console.log(context,param);
            return new Promise((resolve, reject) => {
                getLoginUsers(param).then(res => {
                    console.log(res);
                }).catch(error => {
                    reject(error);
                })
            })
        }
    }
});

export default store;