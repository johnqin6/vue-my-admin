// import Vue from 'vue'
// import Router from 'vue-router'

import Login from '@/components/login';
import Home from '@/views/home';
import NotFound from '@/views/404.vue';
import Index from '@/views/index.vue';
import userList from '@/views/users/list.vue';
import Main from '@/components/main';
import markdown from '@/components/markdown';
// Vue.use(Router)

const router = [
    {
      path: '/',
      name: '',
      component: Home,
      children: [
        { path: 'index', name: '首页', component: Index}
      ]
    },
    {
      path: '/',
      name: '用户管理',
      component: Home,
      children: [
        { path: 'userlist', name: '用户信息', component: userList}
      ]
    },
    {
      path: '/',
      name: '账户管理',
      component: Home,
      children: [
        { path: 'accountlist', name: '账户信息', component: Main}
      ]
    },
    {
      path: '/',
      name: '组件应用',
      component: Home,
      children: [
        { path: 'markdown', name: 'markdown', component: markdown}
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/404',
      name: 'NotFound',
      hidden: true,
      component: NotFound
    },
    {
      path: '*',
      hidden: true,
      redirect: { path: '/404'}
    }
]

export default router;