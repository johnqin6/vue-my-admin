import { rejects } from 'assert';

const XLSX = require('xlsx');
export default {
    importf({inputDOM, obj, rABS = false }) {
            let outdata = [];
            let _this = this;
            //let inputDOM = this.$refs.inputer;   // 通过DOM取文件数据
            this.file = event.currentTarget.files[0];         
            //var rABS = false; //是否将文件读取为二进制字符串        
            var f = this.file;       
            var reader = new FileReader();       
            //if (!FileReader.prototype.readAsBinaryString) {
        
            FileReader.prototype.readAsBinaryString = function(f) {
                var reader = new FileReader();
                var binary = "";     
                var wb; //读取完成的数据
                reader.onload = function(e) {
                    var bytes = new Uint8Array(reader.result);
                    var length = bytes.byteLength;
                    for(var i = 0; i < length; i++) {
                         binary += String.fromCharCode(bytes[i]);
                    }
                    if(rABS) {
                       wb = XLSX.read(btoa(fixdata(binary)), { //手动转化 
                              type: 'base64'   
                         }); 
                    } else {  
                         wb = XLSX.read(binary, {
                              type: 'binary'  
                         });
                    }
                    outdata = XLSX.utils.sheet_to_json(wb.Sheets[wb.SheetNames[0]]);
                };
                reader.onload();
                reader.readAsArrayBuffer(f);
             }
        
             if(rABS) {
                  reader.readAsArrayBuffer(f);
             } else {
                  reader.readAsBinaryString(f);
             }
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    resolve(outdata);
                },500);
            });
    },
    // 将对应的中文key转化为自己想要的英文key
    dateTransition(outdata,headerList){
         // outdata就是你想要的东西 excel导入的数据       
        // excel 数据再处理
        let arr = []
        outdata.map(v => {  
            let obj = {}    
            for(let k in headerList){
                obj[headerList[k]] = v[k];
            }
          arr.push(obj);
        });
        return arr;
    },
};
    