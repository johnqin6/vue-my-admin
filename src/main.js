// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App'
import routes from './router'

import axios from 'axios';
Vue.prototype.$http = axios
import store from './store'

import Mock from './mock'
Mock.bootstrap();

import elementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

//清除样式
import 'normalize.css/normalize.css'
//引用字体图标
import './styles/icon.css'

Vue.config.productionTip = false

Vue.use(VueRouter)
Vue.use(elementUI);

const router = new VueRouter({
  routes
})

//注册一个全局的 before 钩子，对于需要登录才能访问的模块进行控制
router.beforeEach((to, from, next) => {
  if(to.path == '/login' ){
    localStorage.removeItem('user');
  }
  let user = JSON.parse(localStorage.getItem('user'));
  if(!user && to.path != '/login' || to.path == '/'){
    next({ path: '/login'});
  }else{
    next();
  }
});


/* eslint-disable no-new */
new Vue({
  // el: '#app',
  router,
  store,
  render: h => h(App)
  // components: { App },
  // template: '<App/>'
}).$mount('#app')
