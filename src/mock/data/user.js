import Mock from 'mockjs';

const Random = Mock.Random; //获取random对象，随机生成各种数据

//登录用户数据
const loginUser = [
    {
        id: 10001,
        username: 'admin',
        password: '123456',
        rname: '秦振坤', //真实姓名
        role: 'admin', //权限
        avatar: 'logo.png',
        phone: '15234312501'
    },
    {
        id: 10002,
        username: 'edit1',
        password: '123456',
        rname: '刘邦', //真实姓名
        role: 'edit', //权限
        avatar: 'logo.png',
        phone: '15234312503'
    },
    {
        id: 10003,
        username: 'edit2',
        password: '123456',
        rname: '张三', //真实姓名
        role: 'edit', //权限
        avatar: 'logo.png',
        phone: '15234312505'
    },
];

//随机生成用户数据
const users = [];

for(let i = 0; i < 1000; i++){
    users.push({
        id: Random.guid(), //随机生成一个 GUID。
        name: Random.cname(), //随机生成一个常见的中文姓名。
        // address: Random.county(true), //随机生成一个（中国）县，布尔值：是否生成所属的省，市
        birth: Random.date(), //随机生成一个日期
        sex: Random.integer(0,1), //随机生成一个整数,0代表女，1代表男
        age: Random.integer(10,100),  //随机生成一个整数，代表年龄
        phone: Random.integer(10000000000,19999999999), //随机生成一个手机电话
        province: Random.province(), //随机一个省
        city: Random.city(), //随机一个城市
        county: Random.county(), //随机一个区县
        address: '测试地址',
        creaton: Random.datetime(), //随机一个创建日期
    });
}

export { loginUser, users };