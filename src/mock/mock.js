//import引入方式
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import Mock from 'mockjs';
const Random = Mock.Random; //获取random对象，随机生成各种数据
//require引入方式
// const axios = require('axios');
// const  MockAdapter = require('axios-mock-adapter');

//引入数据
import { loginUser, users } from './data/user';
import cityDate from './data/city';
import utils from '@/utils/utils.js';

let _users = users;
export default {
    /**
     * mock bootstrap
     */
    bootstrap() {
        let mock = new MockAdapter(axios); //设置模拟调试器实例
        //模拟任意GET请求到/loginUser
        //reply的参数为（status, data, headers）

        //mock success reqest
        mock.onGet('/success').reply(200, {
            msg: 'success'
        });
        //mock error reqest
        mock.onGet('/error').reply(200,{
            msg: 'error'
        });
        //请求验证码
        mock.onGet('getCaptcha').reply(config => {
            let captcha = utils.createCaptcha();
            return new Promise((resolve,reject) => {
                setTimeout(() => {
                    resolve([200,{code: 200, captcha}]);
                },500);
            });
        });
        //请求登录
        mock.onPost('/login').reply(config => {
            //config是axios config
            //返回一个数组[status, data, headers]
            let { account, password } = JSON.parse(config.data);
            return new Promise((resolve, reject) => {
                let user = null;
                setTimeout(() => {
                    let hasUser = loginUser.some( u => {
                        let pwd = utils.CalcuMD5(u.password);
                        if(u.username === account && pwd === password){
                            user = JSON.parse(JSON.stringify(u));
                            return true;
                        }
                    })
                    if(hasUser){
                        resolve([200, {code: 200, msg: '请求成功', user}]);
                    }else{
                        resolve([200, {code: 500, msg: '账号或密码不存在'}]);
                    }
                },1000);
            }).catch(error => {
                reject(error);
            });
        });
        //请求修改密码
        mock.onPost('/editPassword').reply(config => {
            let { username, password, newPassword, phone } = JSON.parse(config.data);
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    let _loginUser = loginUser.map(u => {
                        if(phone){
                            if(u.username === username && u.phone === phone){
                                u.password = newPassword
                                resolve([200, {code: 200, msg: '密码重置成功！'}]);
                            }else{
                                resolve([200, {code: 200, msg: '账户或手机号码不正确！'}]);
                            }
                        }else{
                            if(u.username === username && u.password === password){
                                if(password === newPassword){
                                    resolve([200, {code: 200, msg: '新密码不能与原密码相同！'}]);
                                }else{
                                    u.password = newPassword
                                    resolve([200, {code: 200, msg: '密码修改成功！'}]);
                                }
                            }else{
                                resolve([200, {code: 200, msg: '账户或密码不正确！'}]);
                            }
                        }
                        
                        return u;
                    });
                    console.log(_loginUser);
                },500);
            }).catch(err => {
                reject(err);
            });
        });
        //请求用户列表数据
        mock.onGet('/getUserListData').reply(config => {
            let { filter, pageIndex, pageSize } = config.params;
            let username = filter.name;
            _users = utils.objinArrSort(_users,'creaton');
            let filterUserData = _users.filter((user,index) => {
                if(username === ''){
                    return user;
                }else if(user.name.includes(username)){
                    return user;
                } 
            });
            let mockUserData = filterUserData.filter((user,index) => {
                if(index >= (pageIndex - 1) * pageSize && index < pageIndex * pageSize){
                    return user;
                }
            });
            let total = filterUserData.length;
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    resolve([200,{total: total, users: mockUserData}]);
                },500);
            });
        });
        //请求用户数据
        mock.onGet('/getUserData').reply(config => {
            let userid = config.params;
            let userData = _users.filter(user => {
                return user.id === userid;
            });
            return new Promise((resolve,reject) => {
                setTimeout(() => {
                    resolve([200,{user: userData[0]}]);
                },500);
            }).catch(err => {
                reject(err);
            });
        });
        //请求新增用户数据
        mock.onPost('/addUserData').reply(config => {
            let userObj = JSON.parse(config.data);
            _users.push({
                id: Random.guid(),
                name: userObj.name,
                birth: userObj.birth,
                sex: userObj.sex,
                age: userObj.age,
                phone: userObj.phone,
                province: userObj.province,
                city: userObj.city,
                county: userObj.county,
                address: userObj.address,
                creaton: new Date()
            });
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    resolve([200,{code: 200, msg: '新增用户成功！'}]);
                },500);
            });
        });
        //请求编辑用户数据
        mock.onPost('/editUserData').reply(config => {
            let userObj = JSON.parse(config.data);
            _users = _users.map(u => {
                if(u.id === userObj.id){
                    u.name = userObj.name;
                    u.sex = userObj.sex;
                    u.phone = userObj.phone;
                    u.address = userObj.address;
                    u.birth = userObj.birth;
                    u.province = userObj.province;
                    u.city = userObj.city;
                    u.county = userObj.county;
                    u.creaton = new Date();
                }
                return u;
            });
            return new Promise((resolve,reject) => {
                setTimeout(() => {
                    resolve([200,{code: 200, msg: '更新成功！'}]);
                },500);
            });
        });
        //请求删除用户数据
        mock.onGet('/delUserData').reply(config => {
            let delUserList = config.params;
            _users = _users.filter(user => {
                return !delUserList.includes(user.id);
            });
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    resolve([200,{code: 200, msg: '删除成功'}]);
                },500);
            });
        });
        //请求城市数据 -- 根据id返回对应的数据
        mock.onGet('/getCitysData').reply(config => {
            let { provinceId, cityId, countyId } = config.params;
            var provinceArr = [],
                cityArr = [],
                countyArr = [];
            if(provinceId === ''){
                cityDate.forEach(province => {
                    provinceArr.push({
                        Code: province.Code,
                        Name: province.Name
                    });
                });
            }else if(cityId === ''){
                cityDate.forEach(province => {
                    if(province.Code === provinceId){
                        province.level.forEach(city => {
                            if(city.level !== '' || city.level.length === 1){
                                cityArr.push({
                                    Code: city.Code,
                                    Name: city.Name
                                });
                            }else{
                                cityArr.push({
                                    Code: city.Code,
                                    Name: city.Name,
                                    islevel: true
                                });
                            }
                        });
                    }
                });
            }else if(provinceId && cityId){
                cityDate.forEach(province => {
                    if(province.Code === provinceId){
                        province.level.forEach(city => {
                            if(city.Code === cityId){
                                city.level.forEach(county => {
                                    countyArr.push({
                                        Code: county.Code,
                                        Name: county.Name,
                                        islevel: true
                                    });
                                });
                            }
                        });
                    }
                }); 
            }
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    let citys = [];
                    if(provinceId === ''){
                        citys = provinceArr;
                    }else if(cityId === ''){
                        citys = cityArr;
                    }else{
                        citys = countyArr;
                    }
                    let total = citys.length;
                    resolve([200,{total: total, cityList: citys}]);
                },500);
            }).catch(error => {
                reject(error);
            });
        });
        //请求城市数据 -- 返回全部数据
        mock.onGet('/getAllCitysData').reply(config => {
            cityDate.forEach(item => {
                item.level.forEach(city => {
                    if(!city.level || city.level[0].Name === ''){
                        city.level = '';
                    }
                });
            });
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    resolve([200,{ cityList: cityDate}]);
                },500);
            }).catch(error => {
                reject(error);
            });
        })
    }
}

